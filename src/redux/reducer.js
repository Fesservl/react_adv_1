/**
 * Created by Sergey on 10.06.2018.
 */
import {combineReducers} from 'redux'
import {routerReducer as router} from 'react-router-redux'                                  //  для роутинга в redux
import {reducer as form} from 'redux-form'                                                  //  для работы с фомами
import auth, {moduleName as authModule} from '../ducks/auth'

export default combineReducers({
    router,
    form,                                                                                    //  для работы как из коробки необходимо назвать form
    [authModule]:auth
})