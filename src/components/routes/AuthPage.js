import React, {Component} from 'react'
import SignInForm from '../auth/SignInForm'
import SignUpForm from '../auth/SignUpForm'
import {Route, NavLink} from 'react-router-dom'
import {connect} from 'react-redux'
import {signUp, moduleName} from '../../ducks/auth'
import Loader from '../common/Loader'

class AuthPage extends Component {

    render() {
        const {loading} = this.props;
        return (
            <div>
                <h1>Auth page</h1>
                <NavLink to='/auth/singin' activeStyle={{color: 'green'}}>Sign In</NavLink>
                <NavLink to='/auth/singup' activeStyle={{color: 'green'}}>Sign Up</NavLink>
                <Route path='/auth/singin' render={() => <SignInForm onSubmit={this.handleSingIn}/>}/>
                <Route path='/auth/singup' render={() => <SignUpForm onSubmit={this.handleSingUp}/>}/>
                {loading && <Loader/>}
            </div>
        )
    }

    handleSingIn = (values) => console.log('___', values)
    handleSingUp = ({email,password}) => this.props.signUp(email,password)
}

export default connect((state) => ({
    loading:state[moduleName].loading
}),{signUp})(AuthPage)