import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker'; // помогает кэшировать ваши активы и другие файлы
import './config'


ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
