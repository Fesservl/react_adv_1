import firebase from 'firebase'

export const  appName = "advreact-12-06";
export const firebaseConfig = {
    apiKey: "AIzaSyABKxo7j_qbPlO7qYkQ35KAb4C7OAvktmA",
    authDomain: `${appName}.firebaseapp.com`,
    databaseURL: `https://${appName}.firebaseio.com`,
    projectId: appName,
    storageBucket: `${appName}.appspot.com`,
    messagingSenderId: "1017993989012"
};

firebase.initializeApp(firebaseConfig)
