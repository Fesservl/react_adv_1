/**
 * Created by Sergey on 10.06.2018.
 */
import createHistory from 'history/createBrowserHistory'

export default createHistory()